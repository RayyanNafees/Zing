# Zing

The new social media app for Gen-Z

## Lib's to take care of

* [Curved bottom bar](https://www.npmjs.com/package/react-native-curved-bottom-bar)

* [Animated curved bottom bar](https://www.npmjs.com/package/@mindinventory/react-native-tab-bar-interaction)

* [Element Transitions](https://www.npmjs.com/package/react-native-shared-element)

* [Screen Transitions](https://www.npmjs.com/package/react-navigation-shared-element)

* [Animation](https://www.npmjs.com/package/react-native-reanimated)

* [Audio player](https://www.npmjs.com/package/react-native-audio-recorder-player)

* [Realm](realm.io)

* dotenv
* snap-carousel
* date-time-picker

## Finger prints

**SHA1:** 5E:8F:16:06:2E:A3:CD:2C:4A:0D:54:78:76:BA:A6:F3:8C:AB:F6:25
**SHA-256:** FA:C6:17:45:DC:09:03:78:6F:B9:ED:E6:2A:96:2B:39:9F:73:48:F0:BB:6F:89:9B:83:32:66:75:91:03:3B:9C

----------------------------------------------------------------------

**SHA1:** 5E:8F:16:06:2E:A3:CD:2C:4A:0D:54:78:76:BA:A6:F3:8C:AB:F6:25
**SHA-256:** FA:C6:17:45:DC:09:03:78:6F:B9:ED:E6:2A:96:2B:39:9F:73:48:F0:BB:6F:89:9B:83:32:66:75:91:03:3B:9C