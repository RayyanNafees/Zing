import {View, Text, Image} from 'react-native';
import React from 'react';
import {backgrey} from '../constants/colors';
import logo from '../assets/logo.png';
import {Row, Touchable} from '../styles/General';

const Button = ({title, icon}) => (
  <Touchable>
    <Row width="70%" style={{backgroundColor: 'white'}}>
      <Image source={icon} style={{height:50, width:50}}/>
      <Text>{title}</Text>
    </Row>
  </Touchable>
);

const Login = () => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: backgrey,
      }}>
      <Row style={{marginHorizontal: '5%'}}>
        <Image
          source={logo}
          style={{
            height: 200,
            width: 200,
          }}
        />
      </Row>
    </View>
  );
};

export default Login;
