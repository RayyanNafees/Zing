import styled from 'styled-components/native';
import {
  TouchableNativeFeedback,
  TouchableOpacity,
  Platform,
  Image,
  ImageBackground,
} from 'react-native';

export const Row = styled.View`
  justify-content: ${props => props.justify || 'center'};
  align-items: ${props => props.align || 'center'};
  flex-direction: row;
  width: ${props => props.wisth || '100%'};
`;

export const Touchable =
  Platform.OS === 'android' ? TouchableNativeFeedback : TouchableOpacity;

export const Img = ({src, height, width, children, ...props}) => {
  const ImageComp = children ? ImageBackground : Image;

  return (
    <ImageComp
      style={{height, width}}
      source={typeof src === 'string' ? {uri: src} : src}
      {...props}
    />
  );
};

